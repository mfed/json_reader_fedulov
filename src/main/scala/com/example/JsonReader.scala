package com.example

import org.apache.spark.{SparkConf, SparkContext}

import java.nio.file.{Files, Paths}
import io.circe._
import io.circe.generic.auto._
import org.apache.log4j.{Level, Logger}

object JsonReader {
  def main(args: Array[String]): Unit = {
    if (args.length == 0) {
      println("Nothing to do")
      return
    }
    val pathToFile = args(0)

    if (!Files.exists(Paths.get(pathToFile))) {
      println("File not found")
      return
    }

    Logger.getLogger("org").setLevel(Level.ERROR)
    val conf = new SparkConf()
      .setAppName("JsonReader")
      //.setMaster("local")

    val sc = new SparkContext(conf)

    sc
      .textFile(pathToFile)
      .map(s => parser.decode[Wine](s) match {
        case Right(wine) => wine
        case Left(ex) => ex
      })
      .foreach(println)

    sc.stop()
  }
}
