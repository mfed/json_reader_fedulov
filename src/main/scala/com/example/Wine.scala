package com.example

case class Wine(
               id: Option[Int],
               country: Option[String],
               title: Option[String],
               variety: Option[String],
               winery: Option[String],
               points: Option[Int],
               price: Option[Double]
               ) {
  override def toString: String = {
    f"${id.getOrElse(null)}\t$country\t$title\t$variety\t$winery\t${points.getOrElse(null)}\t${price.getOrElse(Double.NaN)}%.2f"
  }
}


